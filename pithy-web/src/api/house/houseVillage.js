import request from '@/utils/request'

// 查询列表
export function listHouseVillage(query) {
  return request({
    url: '/system/houseVillage/list',
    method: 'get',
    params: query
  })
}

// 查询详细
export function getHouseVillage(id) {
  return request({
    url: '/system/houseVillage/' + id,
    method: 'get'
  })
}

// 新增
export function addHouseVillage(data) {
  return request({
    url: '/system/houseVillage',
    method: 'post',
    data: data
  })
}

// 修改
export function updateHouseVillage(data) {
  return request({
    url: '/system/houseVillage',
    method: 'put',
    data: data
  })
}

// 删除
export function delHouseVillage(id) {
  return request({
    url: '/system/houseVillage/' + id,
    method: 'delete'
  })
}
