import request from '@/utils/request'

export function getUrlByResourceIdAndResourceType(resourceId,resourceType) {
  return request({
    url: '/resources/resourceId/'+resourceId+'/resourceType/'+resourceType+"/url",
    method: 'get'
  })
}


export function getResourceByResourceIdAndResourceType(resourceId,resourceType) {
  return request({
    url: '/resources/resourceId/'+resourceId+'/resourceType/'+resourceType+"/resource",
    method: 'get'
  })
}
