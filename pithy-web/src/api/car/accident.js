import request from '@/utils/request'

export function list(query) {
  return request({
    url: 'car/accidents',
    method: 'get',
    params: query
  })
}


// 新增
export function add(data) {
  return request({
    url: 'car/accidents',
    method: 'post',
    data: data
  })
}

// 修改
export function update(id,data) {
  return request({
    url: 'car/accidents/'+id,
    method: 'put',
    data: data
  })
}

// 删除
export function del(id) {
  return request({
    url: 'car/accidents/'+id,
    method: 'delete'
  })
}
