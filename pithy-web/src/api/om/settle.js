import request from '@/utils/request'
export function getModule() {
  return '/settles';
}
export function list(query) {
  return request({
    url: '/settles/contract',
    method: 'get',
    params: query
  })
}


// 新增
export function add(data) {
  return request({
    url: '/settles',
    method: 'post',
    data: data
  })
}

// 修改
export function update(id,data) {
  return request({
    url: '/settles/'+id,
    method: 'put',
    data: data
  })
}

// 删除
export function del(id) {
  return request({
    url: '/settles/'+id,
    method: 'delete'
  })
}
