import request from '@/utils/request'

export function getModule() {
 return '/contracts';
}

export function list(query) {
  return request({
    url: '/contracts',
    method: 'get',
    params: query
  })
}


// 新增
export function add(data) {
  return request({
    url: '/contracts',
    method: 'post',
    data: data
  })
}

// 修改
export function update(id,data) {
  return request({
    url: '/contracts/'+id,
    method: 'put',
    data: data
  })
}

// 删除
export function del(id) {
  return request({
    url: '/contracts/'+id,
    method: 'delete'
  })
}
