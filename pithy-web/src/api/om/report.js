import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/report/generics',
    method: 'get',
    params: query
  })
}


// 新增
export function add(data) {
  return request({
    url: '/report/generics',
    method: 'post',
    data: data
  })
}

// 修改
export function update(id,data) {
  return request({
    url: '/report/generics/'+id,
    method: 'put',
    data: data
  })
}

// 删除
export function del(id) {
  return request({
    url: '/report/generics/'+id,
    method: 'delete'
  })
}
