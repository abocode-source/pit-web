import request from '@/utils/request'

export function search(query) {
  return request({
    url: '/searcher',
    method: 'get',
    params: query
  })
}

export function searchDistinct(query) {
  return request({
    url: '/searcher/distinct',
    method: 'get',
    params: query
  })
}
