import request from '@/utils/request'

export function list(query) {
  return request({
    url: '/transport/offers',
    method: 'get',
    params: query
  })
}

export function exportList(query) {
  return request({
    url: '/transport/offers/export',
    method: 'get',
    params: query
  })
}


// 新增
export function add(data) {
  return request({
    url: '/transport/offers',
    method: 'post',
    data: data
  })
}

// 修改
export function update(id,data) {
  return request({
    url: '/transport/offers/'+id,
    method: 'put',
    data: data
  })
}

// 删除
export function del(id) {
  return request({
    url: '/transport/offers/'+id,
    method: 'delete'
  })
}
