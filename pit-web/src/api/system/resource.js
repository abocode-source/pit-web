import request from '@/utils/request'

export function getUrlByResourceIdAndResourceType(resourceId,resourceType) {
  return request({
    url: '/resources/resourceId/'+resourceId+'/resourceType/'+resourceType,
    method: 'get'
  })
}
